import os
from flask import Flask,jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://postgresql://hello_flask:hello_flask@db:5432/hello_flask_dev"
db = SQLAlchemy(app)
db.create_all()

class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)

    def __init__(self, email):
        self.email = email


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route("/add")
def add_email():
    obj = User(email="email")
    db.session.add(obj)
    db.session.commit()
@app.route('/emails')
def emails():
    return jsonify(User.query.all())


if __name__ == '__main__':
    app.run(debug=True)
