from typing import List, Dict
from flask import Flask, request, Response
import mysql.connector
import json
import time
import prometheus_client
from prometheus_client.core import CollectorRegistry
from prometheus_client import Summary, Counter, Histogram, Gauge

_INF = float("inf")

graphs = {}
graphs['c'] = Counter('python_request_operations_total', 'The total number of processed requests')
graphs['h'] = Histogram('python_request_duration_seconds', 'Histogram for the duration in seconds.', buckets=(1, 2, 5, 6, 10, _INF))

app = Flask(__name__)
config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'devopsroles'
    }

def test_table() -> List[Dict]:
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM test_table')
    results = [{name: color} for (name, color) in cursor]
    cursor.close()
    connection.close()

    return results


@app.route('/')
def index() -> str:
    return "Hello World 21.09.2022"
@app.route('/all')
def all() -> str:
    return json.dumps({'test_table': test_table()})
@app.route('/add')
def add() -> str:
    start = time.time()
    graphs['c'].inc()
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    args = request.args
    cursor.execute(''' INSERT INTO test_table VALUES(%s,%s)''',(args['name'],args['color']))
    connection.commit()
    cursor.close()
    connection.close()
    end = time.time()
    graphs['h'].observe(end - start)
    return json.dumps({'test_table': test_table()})

@app.route("/metrics")
def requests_count():
    res = []
    for k,v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype="text/plain")

def sum(a,b):
    return a+b
if __name__ == '__main__':
    app.run(host='0.0.0.0')
