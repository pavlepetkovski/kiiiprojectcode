import unittest
from app import sum
class Test(unittest.TestCase):
    def test_sum(self):
        value = sum(5,5)
        self.assertEqual(value,10)

if __name__ == '__main__':
    unittest.main()